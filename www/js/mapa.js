 $(document).ready(function() {
    var map = L.map('tf-map', {
    	center: [-19.886514, -44.042754], //lat, long
    	zoom: 9
    });

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
         attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
         maxZoom: 18,
         id: 'cunhamath.omdkf4lp',
         accessToken: 'pk.eyJ1IjoiY3VuaGFtYXRoIiwiYSI6ImNpamNydDlndjAwMmZ1dW0wb3R4bjA3czgifQ.jUJ8GTLrk8O_ADt0WPNtOQ'
    }).addTo(map);

    map.scrollWheelZoom.disable();

    L.marker([-19.766723, -44.089415]).addTo(map)
    .bindPopup('<h4>UPA Joanico</h4><ul><li>Endereço: Rua Jose Pedro Pereira, 75 (Centro) - Ribeirão Das Neves , MG - 33880-620</li><li>Telefone: (31) 3627-7070</li><li>CNES:2181797</li></ul>')

    L.marker([-19.800522, -44.007837]).addTo(map)
    .bindPopup('<h4>UPA Acrizio Menezes</h4><ul><li>Endereço: Av. Denise Cristina da Rocha, 600 - Cerejeira (Justinópolis),Ribeirão das Neves - MG,33940-010</li><li>Telefone: (31)3638-1833</li><li>CNES:6632858</li></ul>')

    L.marker([-19.777758, -44.079236]).addTo(map)
    .bindPopup('<h4>Hospital São Judas Tadeu</h4><ul><li>Endereço: R. Waldemar José Alves, 65 - Status,Ribeirão das Neves - MG,33880-190</li><li>Telefone: (31)3627-5238</li><li>CNES:2756749</li></ul>')

    L.marker([-19.277540, -44.398854]).addTo(map)
    .bindPopup('<h4>Hospital São Vicente de Paulo de Paraopeba</h4><ul><li>Endereço: R. Vander Moreira, 182,Paraopeba - MG,35774-000</li><li>Telefone: (31)3714-1310</li><li>CNES:2126990</li></ul>')

    L.marker([-20.445511, -44.768440]).addTo(map)
    .bindPopup('<h4>Santa Casa de Misericórdia de Cláudio</h4><ul><li>Endereço: Av. Araguaia, 155 - Centro,Cláudio - MG,35530-000</li><li>Telefone: (37)3381-1004</li><li>CNES:2144204</li></ul>')

    L.marker([-19.948854, -44.111820]).addTo(map)
    .bindPopup('<h4>Maternidade Pública Municipal de Betim</h4><ul><li>Endereço: R. Gaturama, 180, Jardim Teresópolis Betim/MG - Cep.: 32680-130</li><li>Telefone: (31)3591-6941 / (31)3591-3469</li><li>CNES:2126508</li></ul>')

    L.marker([-19.929487, -44.067741]).addTo(map)
    .bindPopup('<h4>Maternidade Municipal de Contagem</h4><ul><li>Endereço: Av. João César de Oliveira, 4495 - Novo Eldorado,Contagem - MG,32010-000</li><li>Telefone: (31)3363-5300</li><li>CNES:2191164</li></ul>')

    L.marker([-19.936089, -44.061065]).addTo(map)
    .bindPopup('<h4>Hospital Municipal de Contagem</h4><ul><li>Endereço: Av. João César de Oliveira, 4495 - Novo Eldorado,Contagem - MG,32010-000</li><li>Telefone: (31)3363-5300</li><li>CNES:2200473</li></ul>')

    L.marker([-19.936089, -44.061108]).addTo(map)
    .bindPopup('<h4>Hospital Público Regional de Betim Osvaldo Rezende Franco</h4><ul><li>Endereço: Av. Edméia Matos Lazaroti, 3800 - Jardim da Cidade,Betim - MG,32510-430</li><li>Telefone: (31)3539-8129</li><li>CNES:2126494</li></ul>')

    L.marker([-19.963129, -44.188793]).addTo(map)
    .bindPopup('<h4>UAI Sete de Setembro</h4><ul><li>Endereço: Av. Bandeirantes, 441 - Vila Triangulo,Betim - MG</li><li>CNES:2126001</li></ul>')

    L.marker([-19.948657, -44.123164]).addTo(map)
    .bindPopup('<h4>UAI Teresópolis</h4><ul><li>Endereço: Av. Belo Horizonte, 154 - Jardim Teresopolis,Betim - MG,32681-426</li><li>Telefone: (31)3597-8270</li><li>CNES:2126133</li></ul>')

    L.marker([-19.930158, -44.113583]).addTo(map)
    .bindPopup('<h4>UPA - Unidade de Pronto Atendimento - Petrolândia</h4><ul><li>Endereço: R. Refinária União, 137 - Petrolândia,Contagem - MG,32072-180</li><li>Telefone: (31)3352-5312</li><li>CNES:2190125</li></ul>')

    L.marker([-19.837062, -44.147669]).addTo(map)
    .bindPopup('<h4>UPA Nova Contagem</h4><ul><li>Endereço: Av. Retiro dos Imigrantes - Retiro,Contagem - MG,32050-710</li><li>Telefone: (31)3911-7419</li><li>CNES:2189860</li></ul>')

    L.marker([-20.066501, -44.601299]).addTo(map)
    .bindPopup('<h4>Pronto Socorro de Itaúna</h4><ul><li>Endereço: Av. Dr. Miguel Augusto Goncalves, 1902 - Itaúna - MG,35680-147</li><li>Telefone: (37)3249-5300</li><li>CNES:2105780</li></ul>')

    L.marker([-20.018135, -44.067489]).addTo(map)
    .bindPopup('<h4>Hospital Municipal de Ibirité</h4><ul><li>Endereço: Avenida Sao Paulo, 1104 (Macaubas) 32400-000 Ibirité, Minas Gerais-MG</li><li>Telefone: (31)3533-6110</li><li>CNES:6892256</li></ul>')

    L.marker([-19.465140, -44.239737]).addTo(map)
    .bindPopup('<h4>Hospital Nossa Senhora das Graças</h4><ul><li>Endereço: R. Teófilo Otoní, 224 - Centro,Sete Lagoas - MG,35700-007</li><li>Telefone: (31)2107-6000</li><li>CNES:2206528</li></ul>')

    L.marker([-20.501730, -43.857506]).addTo(map)
    .bindPopup('<h4>Hospital Bom Jesus</h4><ul><li>Endereço: Av. Padre Leonardo, 147 - Centro,Congonhas - MG,36404-000</li><li>Telefone: (31)3732-3206</li><li>CNES:2172259</li></ul>')

    L.marker([-20.501730, -43.857506]).addTo(map)
    .bindPopup('<h4>Itabirito UPA Celso Matos Silva</h4><ul><li>Endereço: Rod. dos Inconfidentes, 55 - Santa Efigênia - Itabirito - MG 35450-000</li><li>CNES:7507631</li></ul>')

    L.marker([-19.240538, -44.023347]).addTo(map)
    .bindPopup('<h4>Centro de Saúde Fidelis Diniz Costa</h4><ul><li>Endereço: R. João Saturnino Lopes, 365 Jequitibá - MG 35767-000</li><li>Telefone: (31)3717-6415</li><li>CNES:2155648</li></ul>')

});