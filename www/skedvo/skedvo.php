<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>RCS - skedvo</title>
        <!-- Favicons
        ================================================== -->
        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css"  href="/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="/fonts/font-awesome/css/font-awesome.css"/>
        <!-- Slider
        ================================================== -->
        <link href="/css/owl.carousel.css" rel="stylesheet" media="screen"/>
        <link href="/css/owl.theme.css" rel="stylesheet" media="screen"/>
        <!-- Stylesheet
        ================================================== -->
        <link rel="stylesheet" type="text/css"  href="style_skedvo.css"/>
        <!-- <link rel="stylesheet" type="text/css" href="/css/responsive.css"/> -->
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'/>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400' rel='stylesheet' type='text/css'/>
        <script type="text/javascript" src="/js/modernizr.custom.js"></script>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- <script type="text/javascript" src="/js/jquery.1.11.1.js"></script> -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="/js/bootstrap.js"></script>
        <script type="text/javascript" src="/js/SmoothScroll.js"></script>
        <script type="text/javascript" src="/js/jquery.isotope.js"></script>
        <script src="/js/owl.carousel.js"></script>
        <!-- Javascripts
        ================================================== -->
        <script type="text/javascript" src="/js/main.js"></script>
    </head>
    <body>
        <!-- zeh i -->
        <form id="form1">
            <!-- Navigation
            ==========================================-->
            <nav id="tf-menu" class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php">Rede de Cuidados de Saúde</a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#" class="page-scroll">Home</a></li>
                            <li><a href="#" class="page-scroll">Sobre o skedvo</a></li>
                            <li><a href="#" class="page-scroll">Equipe</a></li>
                            <li><a href="#" class="page-scroll">Serviços</a></li>
                            <li><a href="#" class="page-scroll"></a></li>
                            <li><a href="#" class="page-scroll">Tutoriais</a></li>
                            <li><a class="page-scroll"></a></li>
                            <li><a class="page-scroll"></a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
                    <!-- Home Page
                    ==========================================-->
            <div id="tf-home" class="text-center">
                <div class="overlay">
                    <div class="content">
                        <h1>Bem-Vindo ao <strong><span class="color">RCS sked</span></strong><span class="xyz">vo</span></h1>
                        <a href="#tf-about" class="fa fa-angle-down page-scroll"></a>
                    </div>
                </div>
            </div>
                    <!-- About Us Page -->
                    
                    <!-- Team Page -->
                    <!-- ========================================== -->
<!--             <div id="tf-team" class="text-center">
                <div class="overlay">
                    <div class="container">
                        <div class="section-title center">
                            <h2>Nossa <strong>equipe</strong></h2>
                            <div class="line">
                                <hr/>
                            </div>
                        </div>
                        <div id="team" class="owl-carousel owl-theme row">
                            <div class="item">
                                <div class="thumbnail">
                                    <img src="img/team/ricardo.jpg" alt="..." class="img-circle team-img"/>
                                    <div class="caption">
                                        <h3>Ricardo Cabral</h3>
                                        <p>Coordenador de Atenção Médica</p>
                                        <p>Sou médico, gestor de saúde há mais de 15 anos.Formado em BH, tenho trabalhado em projetos de instituições de saúde em diversos lugares, com culturas e aprendizados distintos, como na Holanda, Espanha, Inglaterra, Estados Unidos, Singapura, Chile e outros.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <img src="img/team/cristiano.jpg" alt="..." class="img-circle team-img"/>
                                    <div class="caption">
                                        <h3>Cristiano Tavares</h3>
                                        <p>Diretor Administrativo Financeiro</p>
                                        <p>Cristiano é formado em Administração de Empresas e pós graduado pela Faculdade Oswaldo Cruz em Gestão de Negócios.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <nav id="footer">
                <div class="container">
                    <div class="pull-left fnav">
                        <p>ALL RIGHTS RESERVED. COPYRIGHT © 2014. Designed by <a href="https://dribbble.com/shots/1817781--FREEBIE-Spirit8-Digital-agency-one-page-template">Robert Berki</a> and Coded by <a href="https://dribbble.com/jennpereira">Jenn Pereira</a></p>
                    </div>
                </div>
            </nav>
        </form>
    </body>
</html>